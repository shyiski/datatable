import "./App.css";
import { Button, Modal, Table, Spin, Pagination } from "antd";
import moment from "moment";
import { useEffect, useState } from "react";

import "../node_modules/antd/dist/antd.css";

function App() {
  const [state, setState] = useState();
  const [page, setPage] = useState(1);
  const [modal, setModal] = useState();

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Messamgers",
      dataIndex: "accounts",
      key: "accounts",
      render: (accounts) => accounts.map((item) => item.from),
    },
    {
      title: "Last Message Time",
      dataIndex: "lastMessageTime",
      key: "lastMessageTime",
      render: (time) => moment(time).format("MM/DD/YYYY hh:mm:ss A"),
    },
    {
      title: "Orders",
      render: (data) => (
        <Button type="primary" onClick={() => setModal(data.id)}>
          {data.name.split(" ")[0]} Orders
        </Button>
      ),
    },
  ];
  const getData = async (page) => {
    try {
      const response = await fetch(
        `https://api.leeloo.ai/api/v2/people?limit=20&offset=${page * 20 - 20}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-Leeloo-AuthToken":
              "7vchwu3qodzegodb191u197e66sx8zmmuglpry64o3u10p79sjif8k1mmz3fotggybvkxfkti5h47mk9iozmqlaiy87yfncm82rq",
          },
        }
      );
      const json = await response.json();
      json && setState(json);
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const closeModal = () => {
    setModal();
  };

  useEffect(() => {
    getData(page);
  }, [page]);

  const ModalItem = () => {
    const [userData, setUserData] = useState();
    const [loading, setLoading] = useState(true);

    const userColumns = [
      {
        title: "Id",
        dataIndex: "id",
        key: "id",
      },
      {
        title: "Title",
        dataIndex: "title",
        key: "title",
      },
      {
        title: "Price",
        dataIndex: "price",
        key: "price",
      },
      {
        title: "Currency",
        dataIndex: "currency",
        key: "currency",
      },
      {
        title: "Status",
        dataIndex: "status",
        key: "status",
      },
      {
        title: "Updated At",
        dataIndex: "updatedAt",
        key: "updatedAt",
        render: (time) => moment(time).format("MM/DD/YYYY hh:mm:ss A"),
      },
    ];
    const getUserData = async (userId) => {
      try {
        const response = await fetch(
          `https://api.leeloo.ai/api/v2/people/${userId}?include=contactedUsers,orders`,
          {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              "X-Leeloo-AuthToken":
                "7vchwu3qodzegodb191u197e66sx8zmmuglpry64o3u10p79sjif8k1mmz3fotggybvkxfkti5h47mk9iozmqlaiy87yfncm82rq",
            },
          }
        );
        const json = await response.json();
        if (json) {
          setLoading(false);
          setUserData(json);
        }
      } catch (error) {
        console.error("User Date Error:", error);
      }
    };

    console.log(userData);

    useEffect(() => {
      modal && getUserData(modal);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [modal]);

    if (loading) return <Spin size="large" />;

    return userData ? (
      <div>
        <Table
          dataSource={(userData && userData.included.orders) || null}
          columns={userColumns}
          pagination={false}
        />
      </div>
    ) : (
      <>No Data</>
    );
  };

  return (
    <div className="App">
      <Table
        colSpan={20}
        dataSource={(state && state.data) || null}
        columns={columns}
        pagination={false}
      />
      {state && (
        <Pagination
          onChange={(page) => setPage(page)}
          className="pagination"
          total={state.meta.totalCount / 2}
        />
      )}
      {modal && (
        <Modal
          width={"100%"}
          destroyOnClose
          onCancel={closeModal}
          visible={!!modal}
          title={<div>User Orders</div>}
          footer={[
            <Button type="primary" key="back" onClick={closeModal}>
              Close
            </Button>,
          ]}
        >
          <ModalItem />
        </Modal>
      )}
    </div>
  );
}

export default App;
